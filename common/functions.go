package common

import (
	"github.com/fsnotify/fsnotify"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func Shelly(method string, data url.Values) {
    addr := "http://192.168.1.222/relay/0"
    username := "xxx"
    password := "xxx"

    client := &http.Client{}

    req, err := http.NewRequest("POST", addr, strings.NewReader(data.Encode()))
    req.SetBasicAuth(username, password)

    if err != nil {
        log.Fatal("could not create the request:", err)
    }

    for {
        _, err = client.Do(req)

        if err != nil {
            log.Println("can not connect to shelly ", err)
			time.Sleep(30 * time.Second)
        } else {
			break
		}
    }

    // Close the request body after all retries
    req.Body.Close()
}

type EventCallback func(event fsnotify.Event)

func Inotify(path string, callback EventCallback) {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	// Start listening for events.
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				// log.Println("event:", event)
				callback(event)

			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	// Add a path.
	err = watcher.Add(path)
	if err != nil {
		log.Fatal(err)
	}
	// Block main goroutine forever.
	<-make(chan struct{})
}
