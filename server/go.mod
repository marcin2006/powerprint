module server

go 1.21.3

require github.com/fsnotify/fsnotify v1.7.0

require (
	common v0.0.0-00010101000000-000000000000 // indirect
	golang.org/x/sys v0.4.0 // indirect
)

replace common => ../common
