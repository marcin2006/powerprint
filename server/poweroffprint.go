package main

import (
	"common"
	"github.com/fsnotify/fsnotify"
	"log"
	"net/url"
	"os/exec"
	"strings"
	"time"
)

func main() {
	common.Inotify("/var/spool/cups/", func(event fsnotify.Event) {
		if event.Has(fsnotify.Create) {
			log.Println("created file:", event.Name)

			time.Sleep(120 * time.Second)

			for {
				cmd := exec.Command("lpstat", "-p")
				out, err := cmd.Output()
				if err != nil {
					log.Fatal("Could not run lpstat", err)
				}

				if strings.Contains(string(out), "is idle") {
					time.Sleep(15 * time.Second) // print last page
					log.Println("no jobs to print, power off printer")
					common.Shelly("POST", url.Values{"turn": {"off"}})
					break
				} else {
					log.Println("jobs waiting:\n" + string(out))
					time.Sleep(5 * time.Second)
				}
			}
		}
	})
}
