package main

import (
    "github.com/fsnotify/fsnotify"
	"common"
	"log"
	"net/url"
)

func main() {
    common.Inotify("/var/spool/cups/", func(event fsnotify.Event){
    if event.Has(fsnotify.Create) {
        log.Println("Created file:", event.Name)
        log.Println("Power on printer")
        common.Shelly("POST", url.Values{"turn": {"on"}})
    }
})
	// Block main goroutine forever.
	<-make(chan struct{})
}
