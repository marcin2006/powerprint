module client

go 1.21.3

require (
	common v0.0.0-00010101000000-000000000000
	github.com/fsnotify/fsnotify v1.7.0
)

require golang.org/x/sys v0.4.0 // indirect

replace common => ../common
